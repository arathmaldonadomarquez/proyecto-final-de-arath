﻿using proyecto_final_de_Arath.Models;
using System;
using System.Linq;
namespace proyecto_final_de_Arath
{
    class Program
    {
        static void Main(string[] args)
        {
            menu();
        }
        public static void menu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("Seleccione una base de datos a la cual acceder:");
            Console.WriteLine("1)Coordinadores");
            Console.WriteLine("2)Supervisores");
            Console.WriteLine("3)Demostradoras");
            Console.WriteLine("4)Promociones");
            Console.WriteLine("5)Jefes de piso");
            
            string opcionbase = Console.ReadLine();
            
            switch (opcionbase)
            {
                case "1":
                    Console.WriteLine("Base de datos:Coordinadores");
                    Console.WriteLine("Seleccione una accion:");
                    Console.WriteLine("1)Crear");
                    Console.WriteLine("2)Buscar");
                    Console.WriteLine("3)Actualizar");
                    Console.WriteLine("4)Eliminar");
                    string opcionfuncionco = Console.ReadLine();
                    switch (opcionfuncionco)
                    {
                        case "1":
                            Crearcoordinador();
                            break;
                        case "2":
                            buscarcoordinador();
                            break;
                        case "3":
                            actualizarcoordinador();
                            break;
                        case "4":
                            eliminarcoordinador();
                            break;
                    }
                    menu();
                    break;
                case "2":
                    Console.WriteLine("Base de datos:Supervisores");
                    Console.WriteLine("Seleccione una accion:");
                    Console.WriteLine("1)Crear");
                    Console.WriteLine("2)Buscar");
                    Console.WriteLine("3)Actualizar");
                    Console.WriteLine("4)Eliminar");
                    string opcionfuncionsu = Console.ReadLine();
                    switch (opcionfuncionsu)
                    {
                        case "1":
                            Crearsupervisor();
                            break;
                        case "2":
                            buscarsupervisor();
                            break;
                        case "3":
                            actualizarsupervisor();
                            break;
                        case "4":
                            eliminarsupervisor();
                            break;
                    }
                    menu();
                    break;
                case "3":
                    Console.WriteLine("Base de datos:Demosradoras");
                    Console.WriteLine("Seleccione una accion:");

                    Console.WriteLine("1)Crear");
                    Console.WriteLine("2)Buscar");
                    Console.WriteLine("3)Actualizar");
                    Console.WriteLine("4)Eliminar");
                    string opcionfuncionde = Console.ReadLine();
                    switch (opcionfuncionde)
                    {
                        case "1":
                            Creardemostradora();
                            break;
                        case "2":
                            buscardemostradora();
                            break;
                        case "3":
                            actualizardemostradora();
                            break;
                        case "4":
                            eliminardemostradora();
                            break;
                    }
                    menu();
                    break;
                case "4":
                    Console.WriteLine("Base de datos:Promociones");
                    Console.WriteLine("Seleccione una accion:");
                    Console.WriteLine("1)Crear");
                    Console.WriteLine("2)Buscar");
                    Console.WriteLine("3)Actualizar");
                    Console.WriteLine("4)Eliminar");
                    string opcionfuncionpr = Console.ReadLine();
                    switch (opcionfuncionpr)
                    {
                        case "1":
                            Crearpromocion();
                            break;
                        case "2":
                            buscarpromocion();
                            break;
                        case "3":
                            actualizarpromocion();
                            break;
                        case "4":
                            eliminarpromocion();
                            break;
                    }
                    menu();
                    break;
                case "5":
                    Console.WriteLine("Base de datos:Jefes de piso");
                    Console.WriteLine("Seleccione una accion:");
                    Console.WriteLine("1)Crear");
                    Console.WriteLine("2)Buscar");
                    Console.WriteLine("3)Actualizar");
                    Console.WriteLine("4)Eliminar");
                    string opcionfuncionje = Console.ReadLine();
                    switch (opcionfuncionje)
                    {
                        case "1":
                            Crearjefe();
                            break;
                        case "2":
                            buscarjefe();
                            break;
                        case "3":
                            actualizarjefe();
                            break;
                        case "4":
                            eliminarjefe();
                            break;
                    }
                    menu();
                    break;
                
            }
            menu();
        }
        //Coordinadores
        public static void Crearcoordinador() //crear coordinador
        {
            Coordinadores coordinador = new Coordinadores();
            coordinador = llenarcoordinador(coordinador);
            using (xxxContext context = new xxxContext())
            {
                context.Add(coordinador);
                context.SaveChanges();
                Console.WriteLine("Coordinador creado");
            }
        }
            public static void buscarcoordinador()//buscar coordinador
            {
            Console.WriteLine("Buscar coordinador");
            Console.Write("Buscar:");
            string buscar = Console.ReadLine();
            using (xxxContext context = new xxxContext())
            {
                IQueryable<Coordinadores> coordinadores = context.Coordinadores.Where(p => p.Nombre.Contains(buscar));
                foreach(Coordinadores coordinador in coordinadores)
                {
                    
                    Console.WriteLine($"Id:{coordinador.Id}");
                    Console.WriteLine($"Nombre:{coordinador.Nombre}");
                    Console.WriteLine($"Edad:{coordinador.Edad}");
                    Console.WriteLine($"Genero:{coordinador.Genero}");
                    Console.WriteLine($"Telefono:{coordinador.Telefono}");
                    Console.WriteLine($"Direccion:{coordinador.Direccion}");
                    Console.WriteLine($"Escolaridad:{coordinador.Nivestu}");
                }
            }
            }
            public static Coordinadores seleccionarCoordinadores()//actualizar coordinador
            {
            buscarcoordinador();
            Console.Write("seleccione id de coordinador:");
            int id = int.Parse(Console.ReadLine());
            using (xxxContext context = new xxxContext())
            {
                Coordinadores coordinadores = context.Coordinadores.Find(id);
                if (coordinadores ==null)
                {
                    seleccionarCoordinadores();
                }
                return coordinadores;
            }

            }
            public static Coordinadores llenarcoordinador(Coordinadores coordinador)
            {
            Console.WriteLine("Nuevo Coordinador");
           
            Console.Write("Nombre:");
            coordinador.Nombre = Console.ReadLine();

            Console.Write("Edad:");
            coordinador.Edad = int.Parse(Console.ReadLine());
            Console.Write("Genero:");
            coordinador.Genero = Console.ReadLine();
            Console.Write("Telefono:");
            coordinador.Telefono = Console.ReadLine();
            Console.Write("Direccion:");
            coordinador.Direccion = Console.ReadLine();
            Console.Write("Nivel de estudios:");
            coordinador.Nivestu = Console.ReadLine();
            return coordinador;
            }  
            public static void actualizarcoordinador()
            {
            Coordinadores coordinador = seleccionarCoordinadores();
            coordinador = llenarcoordinador(coordinador);
            using (xxxContext context = new xxxContext())
            {
                context.Update(coordinador);
                context.SaveChanges();
                Console.WriteLine("Coordinador actualizado");
            }

            }
        public static void eliminarcoordinador()//eliminar coordinador
        {
            Console.WriteLine("Eliminar Coordinador");
            Coordinadores coordinador = seleccionarCoordinadores();
                
                using(xxxContext context = new xxxContext())
                {
                    context.Remove(coordinador);
                    context.SaveChanges();
                    Console.WriteLine("Coordinador eliminado");
                }
                   
        }


        //demostradoras
        public static void Creardemostradora() //crear demostradora
        {
            Demostradoras demostradora = new Demostradoras();
            demostradora = llenardemostradora(demostradora);
            using (xxxContext context = new xxxContext())
            {
                context.Add(demostradora);
                context.SaveChanges();
                Console.WriteLine("Coordinador creado");
            }
        }
        public static void buscardemostradora()//buscar demostradora
        {
            Console.WriteLine("Buscar demostradora");
            Console.Write("Buscar:");
            string buscar = Console.ReadLine();
            using (xxxContext context = new xxxContext())
            {
                IQueryable<Demostradoras> demostradoras = context.Demostradoras.Where(p => p.Nombre.Contains(buscar));
                foreach (Demostradoras demostradora in demostradoras)
                {

                    Console.WriteLine($"Id:{demostradora.Id}");
                    Console.WriteLine($"Nombre:{demostradora.Nombre}");
                    Console.WriteLine($"Edad:{demostradora.Edad}");
                    Console.WriteLine($"Genero:{demostradora.Genero}");
                    Console.WriteLine($"Telefono:{demostradora.Telefono}");
                    Console.WriteLine($"Direccion:{demostradora.Direccion}");
                    Console.WriteLine($"Escolaridad:{demostradora.Nivestu}");
                }
            }
        }
        public static Demostradoras seleccionarDemostradoras()//actualizar demostradora
        {
            buscardemostradora();
            Console.Write("seleccione id de demostradora:");
            int id = int.Parse(Console.ReadLine());
            using (xxxContext context = new xxxContext())
            {
                Demostradoras demostradoras = context.Demostradoras.Find(id);
                if (demostradoras == null)
                {
                    seleccionarDemostradoras();
                }
                return demostradoras;
            }

        }
        public static Demostradoras llenardemostradora(Demostradoras demostradora)
        {
            Console.WriteLine("Nueva demostradora");

            Console.Write("Nombre:");
            demostradora.Nombre = Console.ReadLine();
            Console.Write("Edad:");
            demostradora.Edad = int.Parse(Console.ReadLine());
            Console.Write("Genero:");
            demostradora.Genero = Console.ReadLine();
            Console.Write("Telefono:");
            demostradora.Telefono = Console.ReadLine();
            Console.Write("Direccion:");
            demostradora.Direccion = Console.ReadLine();
            Console.Write("Nivel de estudios:");
            demostradora.Nivestu = Console.ReadLine();
            return demostradora;
        }
        public static void actualizardemostradora()
        {
            Demostradoras demostradora = seleccionarDemostradoras();
            demostradora = llenardemostradora(demostradora);
            using (xxxContext context = new xxxContext())
            {
                context.Update(demostradora);
                context.SaveChanges();
                Console.WriteLine("demostradora actualizada");
            }

        }
        public static void eliminardemostradora()//eliminar demostradora
        {
            Console.WriteLine("Eliminar demostradoras");
            Demostradoras demostradora = seleccionarDemostradoras();

            using (xxxContext context = new xxxContext())
            {
                context.Remove(demostradora);
                context.SaveChanges();
                Console.WriteLine("Demostradora eliminada");
            }

        }
        //supervisores

        public static void Crearsupervisor() //crear supervisor
        {
            Supervisores supervisor = new Supervisores();
            supervisor = llenarsupervisor(supervisor);
            using (xxxContext context = new xxxContext())
            {
                context.Add(supervisor);
                context.SaveChanges();
                Console.WriteLine("supervisor creado");
            }
        }
        public static void buscarsupervisor()//buscar supervisor
        {
            Console.WriteLine("Buscar supervisor");
            Console.Write("Buscar:");
            string buscar = Console.ReadLine();
            using (xxxContext context = new xxxContext())
            {
                IQueryable<Supervisores> supervisores = context.Supervisores.Where(p => p.Nombre.Contains(buscar));
                foreach (Supervisores supervisor in supervisores)
                {

                    Console.WriteLine($"Id:{supervisor.Id}");
                    Console.WriteLine($"Nombre:{supervisor.Nombre}");
                    Console.WriteLine($"Edad:{supervisor.Edad}");
                    Console.WriteLine($"Genero:{supervisor.Genero}");
                    Console.WriteLine($"Telefono:{supervisor.Telefono}");
                    Console.WriteLine($"Direccion:{supervisor.Direccion}");
                    Console.WriteLine($"Escolaridad:{supervisor.Nivestu}");
                }
            }
        }
        public static Supervisores seleccionarSupervisores()//actualizar supervisor
        {
            buscarsupervisor();
            Console.Write("seleccione id de supervisor:");
            int id = int.Parse(Console.ReadLine());
            using (xxxContext context = new xxxContext())
            {
                Supervisores supervisores = context.Supervisores.Find(id);
                if (supervisores == null)
                {
                    seleccionarSupervisores();
                }
                return supervisores;
            }

        }
        public static Supervisores llenarsupervisor(Supervisores supervisor)
        {
            Console.WriteLine("Nuevo Supervisor");

            Console.Write("Nombre:");
            supervisor.Nombre = Console.ReadLine();

            Console.Write("Edad:");
            supervisor.Edad = int.Parse(Console.ReadLine());
            Console.Write("Genero:");
            supervisor.Genero = Console.ReadLine();
            Console.Write("Telefono:");
            supervisor.Telefono = Console.ReadLine();
            Console.Write("Direccion:");
            supervisor.Direccion = Console.ReadLine();
            Console.Write("Nivel de estudios:");
            supervisor.Nivestu = Console.ReadLine();
            return supervisor;
        }
        public static void actualizarsupervisor()
        {
            Supervisores supervisor = seleccionarSupervisores();
            supervisor = llenarsupervisor(supervisor);
            using (xxxContext context = new xxxContext())
            {
                context.Update(supervisor);
                context.SaveChanges();
                Console.WriteLine("supervisor actualizado");
            }

        }
        public static void eliminarsupervisor()//eliminar supervisor
        {
            Console.WriteLine("Eliminar Supervisor");
            Supervisores supervisor = seleccionarSupervisores();

            using (xxxContext context = new xxxContext())
            {
                context.Remove(supervisor);
                context.SaveChanges();
                Console.WriteLine("supervisor eliminado");
            }

        }
        //promociones
        public static void Crearpromocion() //crear promociones
        {
            Promociones promocion = new Promociones();
            promocion = llenarpromocion(promocion);
            using (xxxContext context = new xxxContext())
            {
                context.Add(promocion);
                context.SaveChanges();
                Console.WriteLine("Promocion creada");
            }
        }
        public static void buscarpromocion()//buscar promocion
        {
            Console.WriteLine("Buscar promocion");
            Console.Write("Buscar:");
            string buscar = Console.ReadLine();
            using (xxxContext context = new xxxContext())
            {
                IQueryable<Promociones> promociones = context.Promociones.Where(p => p.Nombre.Contains(buscar));
                foreach (Promociones promocion in promociones)
                {

                    Console.WriteLine($"Id:{promocion.Id}");
                    Console.WriteLine($"Nombre:{promocion.Nombre}");
                    
                }
            }
        }
        public static Promociones seleccionarPromociones()//actualizar promocion
        {
            buscarpromocion();
            Console.Write("seleccione id de promocion:");
            int id = int.Parse(Console.ReadLine());
            using (xxxContext context = new xxxContext())
            {
                Promociones promociones = context.Promociones.Find(id);
                if (promociones == null)
                {
                    seleccionarPromociones();
                }
                return promociones;
            }

        }
        public static Promociones llenarpromocion(Promociones promocion)
        {
            Console.WriteLine("Nueva Promocion");

            Console.Write("Nombre:");
            promocion.Nombre = Console.ReadLine();

            
            return promocion;
        }
        public static void actualizarpromocion()
        {
            Promociones promocion = seleccionarPromociones();
            promocion = llenarpromocion(promocion);
            using (xxxContext context = new xxxContext())
            {
                context.Update(promocion);
                context.SaveChanges();
                Console.WriteLine("promocion actualizada");
            }

        }
        public static void eliminarpromocion()//eliminar promocion
        {
            Console.WriteLine("Eliminar Promocion");
            Promociones promocion = seleccionarPromociones();

            using (xxxContext context = new xxxContext())
            {
                context.Remove(promocion);
                context.SaveChanges();
                Console.WriteLine("Promocion eliminada");
            }

        }
        //Jefes de piso
        public static void Crearjefe() //crear jefe
        {
            Jefespiso jefe = new Jefespiso();
            jefe = llenarjefe(jefe);
            using (xxxContext context = new xxxContext())
            {
                context.Add(jefe);
                context.SaveChanges();
                Console.WriteLine("Jefe de piso creado");
            }
        }
        public static void buscarjefe()//buscar coordinador
        {
            Console.WriteLine("Buscar Jefe");
            Console.Write("Buscar:");
            string buscar = Console.ReadLine();
            using (xxxContext context = new xxxContext())
            {
                IQueryable<Jefespiso> jefes = context.Jefespiso.Where(p => p.Nombre.Contains(buscar));
                foreach (Jefespiso jefe in jefes)
                {

                    Console.WriteLine($"Id:{jefe.Id}");
                    Console.WriteLine($"Nombre:{jefe.Nombre}");
                    Console.WriteLine($"Telefono:{jefe.Telefono}");
                    Console.WriteLine($"Correo:{jefe.Correo}");
                    Console.WriteLine($"Tienda:{jefe.Tienda}");
                }
            }
        }
        public static Jefespiso seleccionarJefes()//actualizar jefes
        {
            buscarjefe();
            Console.Write("seleccione id de Jefe de piso:");
            int id = int.Parse(Console.ReadLine());
            using (xxxContext context = new xxxContext())
            {
                Jefespiso jefes = context.Jefespiso.Find(id);
                if (jefes == null)
                {
                    seleccionarJefes();
                }
                return jefes;
            }

        }
        public static Jefespiso llenarjefe(Jefespiso jefe)
        {
            Console.WriteLine("Nuevo Jefe de piso");

            Console.Write("Nombre:");
            jefe.Nombre = Console.ReadLine();
            Console.Write("Telefono:");
            jefe.Telefono = Console.ReadLine();
            Console.Write("Correo::");
            jefe.Correo = Console.ReadLine();
            Console.Write("Tienda:");
            jefe.Tienda = Console.ReadLine();
            
            return jefe;
        }
        public static void actualizarjefe()
        {
            Jefespiso jefe = seleccionarJefes();
            jefe = llenarjefe(jefe);
            using (xxxContext context = new xxxContext())
            {
                context.Update(jefe);
                context.SaveChanges();
                Console.WriteLine("Jefe de piso actualizado");
            }

        }
        public static void eliminarjefe()//eliminar jefe
        {
            Console.WriteLine("Eliminar Jefe de piso");
            Jefespiso jefe = seleccionarJefes();

            using (xxxContext context = new xxxContext())
            {
                context.Remove(jefe);
                context.SaveChanges();
                Console.WriteLine("Jefe de piso eliminado");
            }

        }
    }
}
