﻿using System;
using System.Collections.Generic;

namespace proyecto_final_de_Arath.Models
{
    public partial class Demostradoras
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }
        public string Genero { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Nivestu { get; set; }
    }
}
